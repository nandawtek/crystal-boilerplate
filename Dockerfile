FROM crystallang/crystal:latest

RUN apt-get update

ENV MODE development

ENV HOME=/opt/app

RUN mkdir -p $HOME

ADD . $HOME

WORKDIR $HOME

RUN shards install
