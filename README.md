## Explore and define the crystal boilerplate to make a rest api

### Files

**shard.yml** This file is used for install dependencies

**app.sh** It file trigger the app

**spec** This folder contains the test Files

**core** This folder contains the api content

### Run app

This command run app

`docker-compose up`

### Launch test

This command launch all test

`docker-compose exec --env KEMAL_ENV=test core crystal spec`
