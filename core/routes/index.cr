require "kemal"
require "../../.env"
require "./animal"
require "./adopters"

Kemal.run(ENV["PORT"].to_i)
